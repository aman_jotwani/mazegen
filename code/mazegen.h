struct Node
{
	vec2i Pos;
	bool32 IsDiscovered;
	uint8_t Mask;
};

struct Path_Part
{
	vec2i Start;
	vec2i End;
	float BlendFactor;
};

class Maze
{
	static const int sNumRules = 4;
	static const uint32_t sPathSize = gMapSideSquared / 2;
	static const vec2i sRuleset[sNumRules];

	Node Tiles[gMapSide][gMapSide];
	Stack<Node*, gMapSideSquared> GenStack;
	Path_Part Path[sPathSize];
	vec2i TerminalPoint;
	uint32_t NumWaypoints;
	bool32 MazeSolved;
	bool32 MazeGenerated;
public:
	Maze();
	Node* GetFirstUndiscoveredChild(Node* Parent);
	void SolveMaze();
	void PlaceAllWalls();
	void ClearPath();
	void ToggleTerminalPoint(Sint32 MouseX, Sint32 MouseY);
	Node* GetRandomUndiscoveredChild(Node* Parent);
	void RemoveWalls(Node* Start, Node* End);
	bool IsTraversible(Node* Start, Node* End);
	Node* GetTile(const vec2i& Coords);
	void GenMaze();
	void HandleEvents(const SDL_Event& Event);
	void UpdateAndRender(SDL_Renderer* Renderer);
};