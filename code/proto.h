#pragma once
#include <stdint.h>

#ifdef _WIN32
#include <SDL.h>
#elif (__APPLE__ && __MACH__)
#include <SDL2/SDL.h>
#endif

#undef main // to override sdl's main

struct Platform_Data
{
	SDL_Window* Window;
	SDL_Renderer* Renderer;
	uint32_t ScreenWidth;
	uint32_t ScreenHeight;
};

bool IsValid(const vec2i& Coords, int LeastX, int MaxX, int LeastY, int MaxY);

vec2i ApplyRule(const vec2i& Coords, vec2i Rule);

int InitSDL(Platform_Data* Engine,
			char* Title,
			uint32_t Width, uint32_t Height);

bool IsPointInRect(SDL_Point* Point, SDL_Rect* Rect);

bool IsOn(uint8_t Flag, uint8_t Mask);

int Lerp(int Start, int End, float Blend);

uint32_t GetRandomIntBetween(uint32_t Low, uint32_t High);