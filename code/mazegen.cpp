const vec2i Maze::sRuleset[sNumRules] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

Maze::Maze()
{
	PlaceAllWalls();
	ClearPath();

	TerminalPoint = {gMapSide - 1, gMapSide - 1};
	MazeSolved = false;
	MazeGenerated = false;
}

void
Maze::PlaceAllWalls()
{
	for(int Row = 0; Row < gMapSide; ++Row)
	{
		for(int Col = 0; Col < gMapSide; ++Col)
		{
			Tiles[Row][Col] = {{Row, Col}, false, 30};			
		}
	}
}

void
Maze::ClearPath()
{
	for(int i = 0; i < sPathSize; ++i)
	{
		Path[i] = {{0, 0}, {0, 0}, 0.0f};
	}
	NumWaypoints = 0;

	for(int Row = 0; Row < gMapSide; ++Row)
	{
		for(int Col = 0; Col < gMapSide; ++Col)
		{
			Tiles[Row][Col].IsDiscovered = false;
		}
	}	
}

void
Maze::HandleEvents(const SDL_Event& Event)
{
	if(Event.type == SDL_KEYDOWN)
	{
		switch(Event.key.keysym.sym)
		{
			case SDLK_g:
			{
				GenMaze();
			}
			break;

			case SDLK_p:
			{
				SolveMaze();
			}
			break;
		}
	}
	else if(Event.type == SDL_MOUSEBUTTONDOWN)
	{
		if(Event.button.button == SDL_BUTTON_LEFT)
		{
			ToggleTerminalPoint(Event.button.x, Event.button.y);
		}
	}
}

bool
Maze::IsTraversible(Node* Start, Node* End)
{
	vec2i diff = End->Pos - Start->Pos;

	if(diff == sRuleset[0]) // {1, 0}, right wall of start
	{
		if(!IsOn(Start->Mask, HasRightWall) &&
		   !IsOn(End->Mask, HasLeftWall))
		{
			return(true);
		}
	}
	else if(diff == sRuleset[1]) // {-1, 0}, left wall of start
	{
		if(!IsOn(Start->Mask, HasLeftWall) &&
		   !IsOn(End->Mask, HasRightWall))
		{
			return(true);
		}
	}
	else if(diff == sRuleset[2]) // {0, 1}, lower wall of start
	{
		if(!IsOn(Start->Mask, HasLowerWall) &&
		   !IsOn(End->Mask, HasUpperWall))
		{
			return(true);
		}
	}
	else if(diff == sRuleset[3]) // {0, -1}, upper wall of start
	{
		if(!IsOn(Start->Mask, HasUpperWall) &&
		   !IsOn(End->Mask, HasLowerWall))
		{
			return(true);
		}
	}
	return(false);
}

Node*
Maze::GetFirstUndiscoveredChild(Node* Parent)
{
	for(uint32_t i = 0; i < sNumRules; ++i)
	{
		Node* child = GetTile(ApplyRule(Parent->Pos, sRuleset[i])); 
		if(child && IsTraversible(Parent, child) &&
		   !(child->IsDiscovered))
		{
			return(child);
		}
	}
	return(NULL);
}

void
Maze::ToggleTerminalPoint(Sint32 MouseX, Sint32 MouseY)
{
	vec2i coords = {MouseX / gBlockWidth, MouseY / gBlockWidth};
	if(coords == TerminalPoint)
	{
		TerminalPoint = {-1, -1};
	}
	else
	{
		TerminalPoint = coords;
	}
}

void
Maze::SolveMaze()
{
	if(IsValid(TerminalPoint, 0, gMapSide, 0, gMapSide) && MazeGenerated)
	{
		ClearPath();

		bool done = false;
		vec2i InitialPoint = {0, 0};

		GenStack.Push(GetTile(InitialPoint));

		while(!done)
		{
			Node* tos = GenStack.Peek();
			Node* child = GetFirstUndiscoveredChild(tos);

			if(child)
			{
				child->IsDiscovered = true;
				GenStack.Push(child);

				if(child->Pos == TerminalPoint)
				{
					done = true;
				}
			}
			else
			{
				GenStack.Pop();
			}
		}

		// transfer stack into an array

		int path_size = GenStack.GetCurrSize();
		vec2i* arr = (vec2i*)(malloc(sizeof(vec2i)*path_size));
		int counter = path_size - 1; // array indexing starts at 0
		while(!(GenStack.IsEmpty()))
		{
			arr[counter--] = (GenStack.Pop())->Pos;
		}

		// create the waypoint structure
		
		int waypoint_index = 0;
		Path[waypoint_index].Start = arr[0];
		Path[waypoint_index].End = arr[1];
		vec2i prev_diff = arr[1] - arr[0];
		for(int i = 1; i < path_size - 1; ++i)
		{
			vec2i curr_diff = arr[i+1] - arr[i];
			if(curr_diff == prev_diff)
			{
				Path[waypoint_index].End += curr_diff;
			}
			else
			{
				waypoint_index += 1;
				Path[waypoint_index].Start = arr[i];
				Path[waypoint_index].End = arr[i+1];
				prev_diff = curr_diff;
			}
		}
		NumWaypoints = waypoint_index + 1;

		// convert waypoints to screen coords

		vec2i half_block_width = {gBlockWidth / 2, gBlockWidth / 2};
		for(int i = 0; i < NumWaypoints; ++i)
		{
			Path[i].Start = Path[i].Start * gBlockWidth + half_block_width;
			Path[i].End = Path[i].End * gBlockWidth + half_block_width;
		}
		free(arr);

		MazeSolved = true;
	}
	else
	{
		SDL_Log("\nYou should first generate the maze by pressing G.");
	}
}

Node*
Maze::GetTile(const vec2i& Coords)
{
	if(IsValid(Coords, 0, gMapSide, 0, gMapSide))
	{
		return(&(Tiles[Coords.x][Coords.y]));
	}
	else
	{
		return(NULL);
	}
}

void
Maze::RemoveWalls(Node* Start, Node* End)
{
	vec2i diff = End->Pos - Start->Pos;

	if(diff == sRuleset[0]) // {1, 0}, right wall of start
	{
		if(IsOn(Start->Mask, HasRightWall) &&
		   IsOn(End->Mask, HasLeftWall))
		{
			Start->Mask ^= HasRightWall;
			End->Mask ^= HasLeftWall;
		}
	}
	else if(diff == sRuleset[1]) // {-1, 0}, left wall of start
	{
		if(IsOn(Start->Mask, HasLeftWall) &&
		   IsOn(End->Mask, HasRightWall))
		{
			Start->Mask ^= HasLeftWall;
			End->Mask ^= HasRightWall;
		}
	}
	else if(diff == sRuleset[2]) // {0, 1}, lower wall of start
	{
		if(IsOn(Start->Mask, HasLowerWall) &&
		   IsOn(End->Mask, HasUpperWall))
		{
			Start->Mask ^= HasLowerWall;
			End->Mask ^= HasUpperWall;
		}
	}
	else if(diff == sRuleset[3]) // {0, -1}, upper wall of start
	{
		if(IsOn(Start->Mask, HasUpperWall) &&
		   IsOn(End->Mask, HasLowerWall))
		{
			Start->Mask ^= HasUpperWall;
			End->Mask ^= HasLowerWall;
		}
	}
}

Node*
Maze::GetRandomUndiscoveredChild(Node* Parent)
{
	Node* UndiscoveredNbrs[sNumRules];
	int nbr_index = 0;
	for(int RuleIndex = 0; RuleIndex < sNumRules; ++RuleIndex)
	{
		Node* t = GetTile(ApplyRule(Parent->Pos, sRuleset[RuleIndex]));
		if(t && !(t->IsDiscovered))
		{
			UndiscoveredNbrs[nbr_index++] = t;
		}
	}

	if(nbr_index == 0)
	{
		return(NULL);
	}
	else
	{
		int rand_index = GetRandomIntBetween(0, nbr_index);
		return(UndiscoveredNbrs[rand_index]);		
	}
}

void
Maze::GenMaze()
{
	PlaceAllWalls();
	ClearPath();

	vec2i InitialPoint = {0, 0};
	Node* InitialNode = GetTile(InitialPoint);
	InitialNode->IsDiscovered = true;
	GenStack.Push(InitialNode);

	while(!(GenStack.IsEmpty()))
	{
		Node* tos = GenStack.Peek();

		Node* next = GetRandomUndiscoveredChild(tos);

		if(next)
		{
			next->IsDiscovered = true;
			RemoveWalls(tos, next);
			GenStack.Push(next);
		}
		else
		{
			GenStack.Pop();
		}
	}

	MazeSolved = false;
	MazeGenerated = true;
}

void
Maze::UpdateAndRender(SDL_Renderer* Renderer)
{
	int HalfWallThickness = gBlockWidth / 10;

	SDL_SetRenderDrawColor(Renderer, 0x00, 0x16, 0x72, 0xFF);
	// draw walls
	for(int Row = 0; Row < gMapSide; ++Row)
	{
		for(int Col = 0; Col < gMapSide; ++Col)
		{
			SDL_Rect temp;

			// using rects to draw thick lines

			if(IsOn(Tiles[Row][Col].Mask, HasRightWall))
			{
				temp = {(Row + 1) * gBlockWidth - HalfWallThickness,
						Col * gBlockWidth,
						HalfWallThickness,
						gBlockWidth};
				SDL_RenderFillRect(Renderer, &temp);
			}

			if(IsOn(Tiles[Row][Col].Mask, HasLeftWall))
			{
				temp = {Row * gBlockWidth,
						Col * gBlockWidth,
						HalfWallThickness,
						gBlockWidth};
				SDL_RenderFillRect(Renderer, &temp);
			}

			if(IsOn(Tiles[Row][Col].Mask, HasUpperWall))
			{
				temp = {Row * gBlockWidth,
						Col * gBlockWidth,
						gBlockWidth,
						HalfWallThickness};
				SDL_RenderFillRect(Renderer, &temp);
			}

			if(IsOn(Tiles[Row][Col].Mask, HasLowerWall))
			{
				temp = {Row * gBlockWidth,
						(Col + 1) * gBlockWidth - HalfWallThickness,
						gBlockWidth,
						HalfWallThickness};
				SDL_RenderFillRect(Renderer, &temp);
			}
		}
	}
	SDL_SetRenderDrawColor(Renderer, 0x00, 0x00, 0x00, 0x00);
	//

	// draw terminal point
	SDL_SetRenderDrawColor(Renderer, 0xff, 0x6c, 0x00, 0xFF);
	SDL_Rect TpointRect = {TerminalPoint.x * gBlockWidth, 
						   TerminalPoint.y * gBlockWidth,
						   gBlockWidth, gBlockWidth};
	SDL_RenderFillRect(Renderer, &TpointRect);
	// 

	if(MazeSolved)
	{
		// draw path
		SDL_SetRenderDrawColor(Renderer, 0xFF, 0x00, 0x00, 0xFF);

		for(int i = 0; i < NumWaypoints; ++i)
		{
			Path_Part* part = &(Path[i]);
			int ix = part->Start.x, iy = part->Start.y, tx, ty;
			if(part->BlendFactor >= 1.0f)
			{
				tx = part->End.x; 
				ty = part->End.y;
			}
			else
			{
				tx = Lerp(part->Start.x, part->End.x, part->BlendFactor);
				ty = Lerp(part->Start.y, part->End.y, part->BlendFactor);
				part->BlendFactor += 0.5f;
				break; // this makes the solution look gradual
			}
			
			SDL_RenderDrawLine(Renderer, ix, iy, tx, ty);
		}

		SDL_SetRenderDrawColor(Renderer, 0x00, 0x00, 0x00, 0x00);		
	}
}