#define global_variable static
#define Assert(Exp) if(!(Exp)) {*(int*)0 = 0;}
#define Kilobytes(Num) 1024*(Num)

#include "containers.h"
typedef int32_t bool32;

#include "proto.h"
#include "proto.cpp"

global_variable const int gMapSide = 8;
global_variable const int gBlockWidth = 100;
global_variable const int gMapSideSquared = gMapSide*gMapSide;

global_variable const uint8_t HasRightWall = 1 << 1;
global_variable const uint8_t HasLeftWall = 1 << 2;
global_variable const uint8_t HasUpperWall = 1 << 3;
global_variable const uint8_t HasLowerWall = 1 << 4;

#include "mazegen.h"
#include "mazegen.cpp"

int main(int argc, char** argv)
{
	Platform_Data sdl;
	bool running = true;
	uint32_t screen_width = gMapSide*gBlockWidth;
	uint32_t screen_height = gMapSide*gBlockWidth;

	if(InitSDL(&sdl, "Maze generation and solver", 
		screen_width, screen_height))
	{
		Maze amaze;

		SDL_Log("Press G to generate maze.");
		SDL_Log("Press P to solve the randomly generated maze.");
		SDL_Log("Left click to set the terminal point, this can be anywhere in the maze.");
		SDL_Log("The initial point is always the top left corner.");

		while(running)
		{
			SDL_Event event;
			while(SDL_PollEvent(&event) != 0)
			{
				if(event.type == SDL_KEYDOWN &&
				   event.key.keysym.sym == SDLK_ESCAPE)
				{
					running = false;
				}
				else
				{
					amaze.HandleEvents(event);
				}
			}

			SDL_SetRenderDrawColor(sdl.Renderer, 0x00, 0x00, 0x00, 0x00);
			SDL_RenderClear(sdl.Renderer);

			amaze.UpdateAndRender(sdl.Renderer);

			SDL_RenderPresent(sdl.Renderer);
		}
	}
	return(0);
}