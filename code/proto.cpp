#include "proto.h"
#include <string.h>
#define _CRT_RAND_S
#include <stdlib.h>
#include <limits.h>

int
InitSDL(Platform_Data* Engine, char* Title, 
        uint32_t Width, uint32_t Height)
{
	Engine->ScreenWidth = Width;
	Engine->ScreenHeight = Height;

    if(SDL_Init(SDL_INIT_EVERYTHING) == 0)
    {
        Engine->Window = SDL_CreateWindow(Title, 
                                          SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                          Engine->ScreenWidth, Engine->ScreenHeight,
                                          SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
        if(Engine->Window)
        {
            Engine->Renderer = SDL_CreateRenderer(Engine->Window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
            if(Engine->Renderer)
            {
                return(1);
            }
            else
            {
                SDL_Log("Renderer creation failed.\n");
                return(0);
            }
        }
        else
        {
            SDL_Log("window creation failed.");
            return(0);
        }        
    }
}

bool 
IsPointInRect(const SDL_Point& Point, const SDL_Rect& Rect)
{
    bool test = Point.x >= Rect.x && Point.x <= Rect.x + Rect.w &&
                Point.y >= Rect.y && Point.y <= Rect.y + Rect.h;

    return(test ? true : false);
}

bool IsValid(const vec2i& Coords, int LeastX, int MaxX, int LeastY, int MaxY)
{
    bool test = Coords.x >= LeastX && Coords.x < MaxX &&
                Coords.y >= LeastY && Coords.y < MaxY;
    
    return(test ? true : false);
}

vec2i ApplyRule(const vec2i& Coords, vec2i Rule)
{
    vec2i result = {Coords.x + Rule.x, Coords.y + Rule.y};
    return(result);
}

int
Lerp(int Start, int End, float Blend)
{
    if(Blend <= 0.0f)
    {
        return(Start);
    }
    else if(Blend >= 1.0f)
    {
        return(End);
    }
    else
    {
        float result = (float)Start + Blend*(End - Start);
        return((int)result);        
    }
}

bool
IsOn(uint8_t Flag, uint8_t Mask)
{
    return((Flag & Mask) == Mask ? true : false);
}

uint32_t
GetRandomIntBetween(uint32_t Low, uint32_t High)
{
    uint32_t RandomNum;
    rand_s(&RandomNum);
    uint32_t Result = (uint32_t) ((double) RandomNum / ((double) UINT_MAX + 1) * High) + Low;
    return(Result);
}