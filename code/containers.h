#include <stdint.h>

struct vec2i
{
	int x, y;

	vec2i operator-(const vec2i& other) const
	{
		vec2i result;
		result.x = x - other.x;
		result.y = y - other.y;
		return(result);
	}	
	
	void operator+=(const vec2i& other)
	{
		x += other.x;
		y += other.y;
	}

	vec2i operator*(const float Scalar) const
	{
		vec2i result;
		result.x = Scalar*x;
		result.y = Scalar*y;
		return(result);
	}

	vec2i operator+(const vec2i& other) const
	{
		vec2i result;
		result.x = x + other.x;
		result.y = y + other.y;
		return(result);
	}

	bool operator==(const vec2i& other) const
	{
		return((x == other.x && y == other.y) ? true : false);
	}
};

template <class GenericType, uint32_t StackSize>
class Stack
{
	GenericType Data[StackSize];
	int TopOfStack;
public:
	Stack();
	int GetMaxSize() const
	{
		return(StackSize);
	};
	int GetCurrSize() const;
	GenericType Pop();
	void Push(GenericType Node);
	GenericType Peek();
	bool IsEmpty() const;
};

template <class GenericType, uint32_t StackSize>
Stack<GenericType, StackSize>::Stack()
{
	TopOfStack = -1;
	for(int i = 0; i < StackSize; ++i)
	{
		Data[i] = {0};
	}
}

template <class GenericType, uint32_t StackSize>
GenericType Stack<GenericType, StackSize>::Pop()
{
	if(TopOfStack >= 0)
	{
		return(Data[TopOfStack--]);
	}
	else
	{
		*(int*)0 = 0;
	}
}

template <class GenericType, uint32_t StackSize>
GenericType Stack<GenericType, StackSize>::Peek()
{
	if(TopOfStack >= 0)
	{
		return(Data[TopOfStack]);
	}
	else
	{
		*(int*)0 = 0;
	}
}

template <class GenericType, uint32_t StackSize>
void Stack<GenericType, StackSize>::Push(GenericType Node)
{
	if(TopOfStack < GetMaxSize() - 1)
	{
		Data[++TopOfStack] = Node;
	}
	{
		// log overflow
	}
}

template <class GenericType, uint32_t StackSize>
bool Stack<GenericType, StackSize>::IsEmpty() const
{
	if(TopOfStack == -1)
	{
		return(true);
	}
	else
	{
		return(false);
	}
}

template <class GenericType, uint32_t StackSize>
int Stack<GenericType, StackSize>::GetCurrSize() const
{
	return(TopOfStack + 1);
}