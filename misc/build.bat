@echo off

if not defined DevEnvDir call "..\\misc\\buildshell.bat"

set INCLUDE=E:\Third-party tools\vs_dev_lib\SDL2-devel-2.0.3-VC\include;%INCLUDE%
set LIB=E:\Third-party tools\vs_dev_lib\SDL2-devel-2.0.3-VC\lib\x86;%LIB%

set CompilerFlags=-MTd -nologo -Gm- -GR- -EHa- -EHs -Zo -FC -Z7 -W3

mkdir ..\build
pushd ..\build
cl %CompilerFlags% ..\code\main.cpp /link SDL2.lib SDL2main.lib -SUBSYSTEM:CONSOLE -NODEFAULTLIB:msvcrt.lib
popd